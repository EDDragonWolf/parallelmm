#ifndef OPENMPMM_H
#define OPENMPMM_H

#include "matrixutils.h"

double **openMPMM(double **a, int rowsA, int columnsA,
                  double **b, int rowsB, int columnsB)
{
    if (columnsA != rowsB)
    {
        return nullptr;
    }
    double **c = MatrixUtils::initMatrix(rowsA, columnsB);
#pragma omp parallel for collapse(2)
//#pragma omp parallel for
    for (int i = 0; i < rowsA; i++)
    {
        for (int j = 0; j < columnsB; j++)
        {
            c[i][j] = 0.0;
            for (int k = 0; k < columnsA; k++)
            {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
    return c;
}

#endif // OPENMPMM_H
