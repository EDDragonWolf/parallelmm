#ifndef SEQUENTIALMM_H
#define SEQUENTIALMM_H

#include "matrixutils.h"

double **sequentialMM(double **a, int rowsA, int columnsA,
                      double **b, int rowsB, int columnsB)
{
    if (columnsA != rowsB)
    {
        return nullptr;
    }
    double **c = MatrixUtils::initMatrix(rowsA, columnsB);
    for (int i = 0; i < rowsA; i++)
    {
        for (int j = 0; j < columnsB; j++)
        {
            c[i][j] = 0.0;
            for (int k = 0; k < columnsA; k++)
            {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
    return c;
}

#endif // SEQUENTIALMM_H
