# High Performance Computing (HPC)

**High-Performance Computing (HPC)** aggregates various technologies which allow to accelerate computations and optimize them.
There are the following approaches to organize HPC:
* Multithread / Concurrent Computing
* Multiprocess Computing
* GPGPU
* Distributed Computing
* Cloud Computing
* Edge Computing
* Hybrid Computing
* Symmetric / Asymmetric computing 

## Multithread / Concurrent Computing

### OpenMP
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/OpenMP_logo.png/270px-OpenMP_logo.png" width="200" />

The application programming interface (API) **OpenMP (Open Multi-Processing)** supports multi-platform 
shared memory multiprocessing programming in C, C++, and Fortran, on many platforms, instruction-set 
architectures and operating systems, including Solaris, AIX, HP-UX, Linux, macOS, and Windows. It 
consists of a set of compiler directives, library routines, and environment variables that influence 
run-time behaviour.

**OpenMP** managed by the nonprofit technology consortium OpenMP Architecture Review Board (or OpenMP 
ARB), jointly defined by a group of major computer hardware and software vendors, including AMD, IBM, 
Intel, Cray, HP, Fujitsu, Nvidia, NEC, Red Hat, Texas Instruments, Oracle Corporation, and more.

**OpenMP** is an implementation of multithreading, a method of parallelizing whereby a master thread 
(a series of instructions executed consecutively) forks a specified number of slave threads and the 
system divides a task among them. The threads then run concurrently, with the runtime environment 
allocating threads to different processors.

#### References
 1. [OpenMP - Wikipedia](https://en.wikipedia.org/wiki/OpenMP).
 2. [OpenMP.org - Official site](https://www.openmp.org/).
 3. [OpenMP - OpenMP Programming Model](https://computing.llnl.gov/tutorials/openMP/#ProgrammingModel).
 4. L. Nyman and M. Laakso, "[Notes on the History of Fork and Join](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7548985)," in IEEE Annals of the History of Computing, vol. 38, no. 3, pp. 84-87, July-Sept. 2016, doi: [10.1109/MAHC.2016.34](https://doi.org/10.1109/MAHC.2016.34).
 5. [Fork/Join. The Java Tutorials](https://docs.oracle.com/javase/tutorial/essential/concurrency/forkjoin.html).
 6. Алексей Колосов, Евгений Рыжков, Андрей Карпов. [32 подводных камня OpenMP при программировании на Си++](https://www.viva64.com/ru/a/0054/).
 7. [openmp - GCC Wiki](https://gcc.gnu.org/wiki/openmp).
 8. [/openmp (Enable OpenMP Support)](https://docs.microsoft.com/en-us/cpp/build/reference/openmp-enable-openmp-2-0-support?view=vs-2019).

## Multiprocess/Distributed Computing

### MPI
<img src="https://6lli539m39y3hpkelqsm3c2fg-wpengine.netdna-ssl.com/wp-content/uploads/2017/05/MPIlogo2.gif" width="200" />

**Message Passing Interface (MPI)** is a standardized and portable message-passing standard 
designed by a group of researchers from academia and industry to function on a wide variety 
of parallel computing architectures. The standard defines the syntax and semantics of a 
core of library routines useful to a wide range of users writing portable message-passing 
programs in C, C++, and Fortran. There are several well-tested and efficient implementations 
of MPI, many of which are open-source or in the public domain. These fostered the development 
of parallel software industry and encouraged the development of portable and scalable 
large-scale parallel applications.

#### References
 1. [MPI - Wikipedia](https://en.wikipedia.org/wiki/Message_Passing_Interface)
 2. [MPI Forum](https://www.mpi-forum.org/)
 3. [William Gropp, Tutorial on MPI: The Message-Passing Interface](http://polaris.cs.uiuc.edu/~padua/cs320/mpi/tutorial.pdf)
 4. [Peter S. Pacheco, A User's Guide to MPI](https://arcb.csc.ncsu.edu/~mueller/cluster/mpi.guide.pdf)

## GPGPU

### CUDA
<img src="https://upload.wikimedia.org/wikipedia/en/b/b9/Nvidia_CUDA_Logo.jpg" width="200" />

**CUDA (Compute Unified Device Architecture)** is a parallel computing platform and application 
programming interface (API) model created by Nvidia. It allows software developers and software 
engineers to use a CUDA-enabled graphics processing unit (GPU) for general-purpose processing – 
an approach termed GPGPU (General-Purpose computing on Graphics Processing Units). The CUDA 
platform is a software layer that gives direct access to the GPU's virtual instruction set and 
parallel computational elements, for the execution of computational kernels.

The CUDA platform designed to work with programming languages such as C, C++, and Fortran. This 
accessibility makes it easier for specialists in parallel programming to use GPU resources, in 
contrast to prior APIs like Direct3D and OpenGL, which required advanced skills in graphics 
programming. CUDA-powered GPUs also support programming frameworks such as OpenACC and OpenCL; 
and HIP by compiling such code to CUDA. When CUDA first introduced by Nvidia, the name was an 
acronym for Compute Unified Device Architecture, but Nvidia subsequently dropped the common 
use of the acronym.

#### References
 1. [CUDA - Wikipedia](https://en.wikipedia.org/wiki/CUDA)
 2. [Nvidia CUDA Home Page](https://developer.nvidia.com/cuda-zone)
 3. [CUDA Code Samples](https://developer.nvidia.com/cuda-code-samples)
 4. [NVIDIA CUDA SDK Code Samples](http://developer.download.nvidia.com/compute/cuda/1.1-Beta/x86_website/samples.html)
 5. [CUDA Tutorial](http://geco.mines.edu/tesla/cuda_tutorial_mio/)
 6. [Massively parallel programming with GPUs](https://people.duke.edu/~ccc14/sta-663/CUDAPython.html)
 7. Sanders, Jason. CUDA by example: an introduction to general-purpose GPU 
 programming / Jason Sanders, Edward Kandrot. 
 [ISBN 978-0-13-138768-3 (pbk. : alk. Paper)](https://www.physics.drexel.edu/~valliere/PHYS405/GPU_Story/CUDA_by_Example_Addison_Wesley_Jul_2010.pdf)
 8. [Віталій Парубочий. Технологія CUDA. Реалізація неграфічних обчислень на графічних процесорах](https://drive.google.com/file/d/1IlJlGI-zk7NQ5AnEH4wDU8-DoG4cO_fB)
 9. [Віталій Парубочий. CUDA C. Основи програмування](https://drive.google.com/file/d/1m74OsCmqVNIg9bFT41Nh77CPUY6i_7Dc)

### OpenCL
<img src="https://www.techpowerup.com/img/gsRWisMF2MyvWza3.jpg" width="200" />

**OpenCL (Open Computing Language)** is a framework for writing programs, that execute across 
heterogeneous platforms consisting of central processing units (CPUs), graphics processing units 
(GPUs), digital signal processors (DSPs), field-programmable gate arrays (FPGAs) and other 
processors or hardware accelerators. OpenCL specifies programming languages (based on C99 and 
C++11) for programming these devices and application programming interfaces (APIs) to control 
the platform and execute programs on the computing devices. OpenCL provides a standard interface 
for parallel computing using task- and data-based parallelism.

**OpenCL** is an open standard maintained by the non-profit technology consortium Khronos Group. 
Conformant implementations are available from Altera, AMD, Apple (OpenCL along with OpenGL 
deprecated for Apple hardware, in favour of Metal 2[7]), ARM, Creative, IBM, Imagination, Intel, 
Nvidia, Qualcomm, Samsung, Vivante, Xilinx, and ZiiLABS.

### SYCL
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Khronos_Group_SYCL_logo.svg/1920px-Khronos_Group_SYCL_logo.svg.png" width="200" />

**SYCL** is a higher-level programming model for OpenCL as a single-source domain-specific 
embedded language (DSEL) based on pure C++11 for SYCL 1.2.1 to improve programming productivity.
SYCL standard developed by Khronos Group announced in March 2014.

#### References
 1. [OpenCL - Wikipedia](https://en.wikipedia.org/wiki/OpenCL)
 2. [SYCL - Wikipedia](https://en.wikipedia.org/wiki/SYCL)
 3. [OpenCL Official Site](https://www.khronos.org/opencl/)