#ifndef MATRIXUTILS_H
#define MATRIXUTILS_H

#include <float.h>

namespace MatrixUtils
{

double **initMatrix(int rows, int columns, double initialValue = 0)
{
    double **m = new double*[rows];
    for (int i = 0; i < rows; ++i)
    {
        m[i] = new double[columns];
        for (int j = 0; j < columns; ++j)
        {
            m[i][j] = initialValue;
        }
    }
    return m;
}

double **fillMatrix(double **m, int rows, int columns)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(1.0, std::nextafter(10.0, DBL_MAX));

    if (!m)
    {
        m = initMatrix(rows, columns);
    }

    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < columns; ++j)
        {
            m[i][j] = dist(mt);
        }
    }
    return m;
}

void printMatrix(double **m, int rows, int columns)
{
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < columns; ++j)
        {
            std::cout << m[i][j] << '\t';
        }
        std::cout << std::endl;
    }
}

}

#endif // MATRIXUTILS_H
