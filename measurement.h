#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#include <chrono>

#define MEASUREMENT_START(x) \
    auto start##x = std::chrono::system_clock::now();

#define MEASUREMENT_END(x, interval) \
    auto end##x = std::chrono::system_clock::now(); \
    auto duration##x = std::chrono::duration_cast<interval>(end##x - start##x).count();

#define MEASUREMENT(x) \
    duration##x

#endif // MEASUREMENT_H
