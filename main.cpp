#include <iostream>
#include <random>
#include "matrixutils.h"
#include "measurement.h"
#include "sequentialmm.h"
#ifdef OPENMPMM
#include "openmpmm.h"
#endif

int main()
{
    int m = 1000;
    int n = 600;
    int k = 500;
    double **A = MatrixUtils::initMatrix(m, n, 1);
//    std::cout << "Matrix A" << std::endl;
//    MatrixUtils::printMatrix(A, m, n);
    double **B = MatrixUtils::initMatrix(n, k, 1);
//    std::cout << "Matrix B" << std::endl;
//    MatrixUtils::printMatrix(B, n, k);
//    std::cout << "Matrix C" << std::endl;
    MEASUREMENT_START(1);
    double **sC = sequentialMM(A, m, n, B, n, k);
    MEASUREMENT_END(1, std::chrono::milliseconds);
    std::cout << "Duration Sequential MM: " << MEASUREMENT(1) << " ms" << std::endl;
//    MatrixUtils::printMatrix(sC, m, k);
#ifdef OPENMPMM
    MEASUREMENT_START(2);
    double **oC = openMPMM(A, m, n, B, n, k);
    MEASUREMENT_END(2, std::chrono::milliseconds);
    std::cout << "Duration OpenMP MM: " << MEASUREMENT(2) << " ms" << std::endl;
//    MatrixUtils::printMatrix(oC, m, k);
#endif

    return 0;
}
